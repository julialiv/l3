#include <string>
#include <map>
#include <set>
#include <vector>
#include <iostream>

using namespace std;

namespace L3 {

	class Instruction;

	enum LineType
	{
		Assign,
		Arith,
		decremIncrem,
		Return,
		lineLabel,
		Comparison,
		conditional_jump,
		call_line,
		leaType
		// ADD MORE HERE
	};

	enum Type
	{
		stackArg,
		mem,
		reg,
		value,
		label,
		operation,
		g2,
		call_type,
		var,
		arrow,
		ret_type,
		arg_type,
		fn_type,
		compare_op
	};

	class Item
	{
		// str will be either a: register or label
		// val will be either a: mem offset or value
		public:
			std::string str;
			int64_t val;
			Type t;

			string toStr() {
				switch (t) {
					case mem:
						return "mem " + str + " " + "0"; //to_string(val);
					case value:
						return to_string(val);
					//case var:
					//	return va
					default:
						return str;
				}
			}
	};

	struct L3_node
	{
		public:
			Item value;
			vector <L3_node*> nodes;
	};

	class Instruction {
		public:
			LineType type;
			std::set<int> successor_set;
			virtual string print() {
				return "return";
			}
			virtual L3_node* convertToTree(){
				struct L3_node* n; 
				return n;
			} 
	};
	
	class Ln_Label: public Instruction {

		public:
			Item lab;
			bool goToFlag;

			string print() {
				string ret = "";
				if (goToFlag)
					ret += "goto ";
				return ret + lab.str;
			}
			L3_node* convertToTree () {
				// Option 1: root node = a label
				L3_node* label_node = new L3_node();
				label_node->value = lab;
				// Root node might be a br
				if (goToFlag){
					Item i;
					i.t = g2;
					L3_node* br = new L3_node();
					br-> value = i;
					br->nodes.push_back(label_node);
					return br;
				}
				return label_node;	
			}
	};

	class Ret: public Instruction
	{
		public:
			Item ret_val;
			bool flag;
			string print() {
					return "return";
			}
			L3_node* convertToTree () {
				Item i;
				i.t = ret_type;
				L3_node* ret_node = new L3_node();
				ret_node->value = i;
				if (flag){
					L3_node* val = new L3_node();
					val->value = ret_val;
					ret_node->nodes.push_back(val);
				}
				return ret_node;
			}
	};

	class Assignment: public Instruction
	{
		public:
			Item dst;
			Item src;

			string print() {
				return dst.toStr() + " <- " + src.toStr();
			}
			L3_node* convertToTree () {
				L3_node* src_node = new L3_node();
				src_node->value = src;
				L3_node* dst_node = new L3_node();
				dst_node->value = dst;
				
				Item i;
				i.t = arrow;
				L3_node *assign_node = new L3_node();	
				assign_node->value = i;
				assign_node->nodes.push_back(dst_node);
				assign_node->nodes.push_back(src_node);
				
				return assign_node;
			}
	};

	class Compare: public Instruction
	{
		public:
			Item src1; // Middle value (always a register)
			Item src2; // Rightmost value
			Item dst;
			Item c;

			string print() {
				return dst.toStr() + " <- " + src1.toStr() + c.toStr() + src2.toStr();
			  }
			L3_node* convertToTree () {
				L3_node* src1_node = new L3_node();
				src1_node->value = src1;
				L3_node* src2_node = new L3_node();
				src2_node->value = src2;
				L3_node* dst_node = new L3_node();
				dst_node->value = dst;
				L3_node* c_node = new L3_node();
				c_node->value = c;
				
				c_node->nodes.push_back(src1_node);
				c_node->nodes.push_back(src2_node);
				

				Item i;
				i.t = arrow;
				L3_node *compare_node = new L3_node();	
				compare_node->value = i;
				compare_node->nodes.push_back(dst_node);
				compare_node->nodes.push_back(c_node);
				
				return compare_node;
			}
	};

	class cJump: public Instruction
	{
		public:
			Item src;
			Item label1;
			Item label2;

			string print() {
				string s = "cjump " + src.toStr();
				s += src.toStr() + " ";
				s += label1.toStr() + " " + label2.toStr();
				return s;
			}
			L3_node* convertToTree () {
				L3_node* label1_node = new L3_node();
				label1_node->value = label1;
				L3_node* label2_node = new L3_node();
				label2_node->value = label2;
				L3_node* src_node = new L3_node();
				src_node->value = src;

				Item i;
				i.t = g2;
				L3_node *cjump_node = new L3_node();	
				cjump_node->value = i;
				cjump_node->nodes.push_back(src_node);
				cjump_node->nodes.push_back(label1_node);
				cjump_node->nodes.push_back(label2_node);
				
				return cjump_node;
			}
	};

	class Arithmetic: public Instruction
	{
		public:
			Item dst;
			Item src1;
			Item src2;
			Item op;

			string print() {
				//cout << "Arithmetic op" << endl;
				string ret = dst.toStr() + " ";
				if (op.str[0] == '<' || op.str[0] == '>') {
					ret.push_back(op.str[0]);
					ret.push_back(op.str[0]);
					ret += "= ";
				}
				else {
					ret.push_back((char)(op.str[0]));
					ret += "= ";
				}
				ret += src1.toStr();
				return ret;
			}

			L3_node* convertToTree () {
				L3_node* dstNode = new L3_node();
				dstNode-> value = dst;
				L3_node* srcNode1 = new L3_node();
				srcNode1-> value = src1;
				L3_node* srcNode2 = new L3_node();
				srcNode2->value = src2;
				L3_node* oper = new L3_node();
				oper->value = op;
				oper->nodes.push_back(srcNode1);
				oper->nodes.push_back(srcNode2);
				
				Item i;
				i.t = arrow;
				L3_node *arith = new L3_node();	
				arith->value = i;
				arith->nodes.push_back(dstNode);
				arith->nodes.push_back(oper);
				
				return arith;	
			}
	};

	// PUT BACK LEA, ETC.

	class Call_inst: public Instruction
	{
		public:
			Item fn;
			int num_args;
			vector<Item> list_args;
			Item dst;
			bool hasDest;

			string print() {
				return "call " + fn.toStr() + " " + to_string(num_args);
			}

			L3_node* convertToTree () {
				Item i;
				i.t =arg_type;
				L3_node *args_node = new L3_node();	
				args_node->value = i;
				for(Item arg : list_args){
					cout << "arg in list args: " << arg.str << endl;
					L3_node* arg_node = new L3_node();
					arg_node->value = arg;
					args_node->nodes.push_back(arg_node);
				}			
	
				L3_node* fn_node = new L3_node();
				fn_node-> value = fn;
				Item i2;
				i2.t =call_type;
				L3_node *call_node = new L3_node();	
				if (hasDest){
					L3_node* dst_node = new L3_node();
					dst_node-> value = dst;
					call_node->nodes.push_back(dst_node);
				}


				call_node->value = i2;
				call_node->nodes.push_back(fn_node);
				call_node->nodes.push_back(args_node);
				
				return call_node;	
			}
	};

	
}
