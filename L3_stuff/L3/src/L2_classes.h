#pragma once

#include <string>
#include <map>
#include <set>
#include <vector>
#include <iostream>

using namespace std;

namespace L3 {

	class L2_Instruction;
	
	struct L2_node
        {
                public:
                        set<Type> types;
			Item value;
                        vector <L2_node*> nodes;
			L2_node( set<Type> t) {
				types = t;
			}
			L2_node(Item value): value(value) {
			}
        };


	class L2_Instruction {
		public:
			LineType type;
			std::set<int> successor_set;
			virtual string print() {
				return "uh oh";
			}
	};

	class L2_Ret : public L2_Instruction {
		string print() {
			return "return";
		}
	};

	class L2_Ln_Label: public L2_Instruction {
		public:
			string lab;
			bool goToFlag;

			L2_Ln_Label(string val, bool flagIn) {
				lab = val;
				goToFlag = flagIn;
			}

			string print() {
				string ret = "";
				if (goToFlag)
					ret += "goto ";
				return ret + lab;
			}
	};

	class L2_LEA: public L2_Instruction {
		public:
			Item dst;
			Item src1;
			Item src2;
			Item offset;
			L2_LEA(Item dst, Item src1, Item src2, Item offset): dst(dst), src1(src1), src2(src2), offset(offset){}
			string print() {
				return dst.toStr() + " @ " + src1.toStr() + " " + src2.toStr() + " " + offset.toStr();
			}
	};

	class L2_Assignment: public L2_Instruction
	{
		public:
			Item dst;
			Item src;
			L2_Assignment(Item dst, Item src): dst(dst), src(src){}
			string print() {
				return dst.toStr() + " <- " + src.toStr();
			}
	};

	class L2_Compare: public L2_Instruction
	{
		public:
			Item src1; // Middle value (always a register)
			Item src2; // Rightmost value
			Item dst;
			Item c;
			L2_Compare(Item src1, Item src2, Item dst, Item c): src1(src1), src2(src2), dst(dst), c(c){}
			string print() {
				return dst.toStr() + " <- " + src1.toStr() + c.toStr() + src2.toStr();
			}
	};

	class L2_cJump: public L2_Instruction
	{
		public:
			Item src1;
			Item src2;
			Item label1;
			Item label2;
			Item c;
			L2_cJump(Item src1, Item src2, Item label1, Item label2, Item c): src1(src1), src2(src2), label1(label1), label2(label2), c(c){}
			string print() {
				string s = "cjump " + src1.toStr();
				s += c.toStr();
				s += src2.toStr() + " ";
				s += label1.toStr() + " " + label2.toStr();
				return s;
			}
	};

	class L2_Arithmetic: public L2_Instruction
	{
		public:
			Item dst;
			Item src;
			Item op;
			L2_Arithmetic(Item dst, Item src, Item op): dst(dst), src(src), op(op){}
			string print() {
				string ret = dst.toStr() + " ";
				ret += op.toStr() + "= ";
				ret += src.toStr();
				return ret;
			}
	};

	class L2_Call_inst: public L2_Instruction
	{
		public:
			Item fn;
			int num_args;
			L2_Call_inst(Item fn, int num_args): fn(fn), num_args(num_args){}
			string print() {
				return "call " + fn.toStr() + " " + to_string(num_args);
			}
	};

	class L2_DI: public L2_Instruction
	{
		public:
			Item dst;
			Item op;
			L2_DI(Item dst, Item op): dst(dst), op(op){}
			string print() {
				return dst.toStr() + op.toStr() + op.toStr();
			}
	};

	
	class L2_tile {
		public:
			vector<L2_Instruction*> instructs;
			L2_node *root;
			virtual L2_tile* clone(){
				cout << "cloning generic L2 tile" << endl;
			}
			virtual void save(map<string,string>* label_names, int* index){
				cout << "saving generic L2 tile " << endl;
			}
			virtual	string print(){
				for (L2_Instruction* i: instructs){
					i->print();
				}
			}		 
	};

	class L2_label_tile : public L2_tile {
		public:
			L2_label_tile(){
				root = new L2_node({label});
			}
			L2_label_tile* clone(){
				return new L2_label_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				if (label_names->find(root->value.str) == label_names->end()){
					cout << "index: " << *index << endl;
					string name = ":call_label" + to_string(*index);
					label_names->insert(pair<string, string>(root->value.str, name));
					*index = *index+1;
				}
				instructs.push_back(new L2_Ln_Label(label_names->at(root->value.str), false));
			}
	};
	class L2_br_label_tile : public L2_tile {
		public:
			L2_br_label_tile(){
				root = new L2_node({g2});
				L2_node *label_node = new L2_node({label});
 		                root->nodes.push_back(label_node);
			}
			L2_br_label_tile* clone(){
				return new L2_br_label_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				if (label_names->find(root->nodes[0]->value.str) == label_names->end()){
					string name = ":call_label" + to_string(*index);
					label_names->insert(pair<string, string>(root->nodes[0]->value.str, name));
					*index=*index+1;
				}
				instructs.push_back(new L2_Ln_Label(label_names->at(root->nodes[0]->value.str), true));
			}
	};
	class L2_assign_tile : public  L2_tile {
		public:
			L2_assign_tile(){
				root = new L2_node({arrow});
				L2_node *dest = new L2_node({var, mem});
				L2_node *source = new L2_node({var, mem, value, operation});
 		                root->nodes.push_back(dest);
 		                root->nodes.push_back(source);
			}
			L2_assign_tile* clone(){
				return new L2_assign_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				instructs.push_back(new L2_Assignment(root->nodes[0]->value, root->nodes[1]->value));
			}

	};
	class L2_arith_tile : public L2_tile {
		public:
			L2_arith_tile(){
				root = new L2_node({arrow});
				L2_node *dest = new L2_node({var, mem});
				L2_node *src1 = new L2_node({var, mem, value, operation});
				L2_node *src2 = new L2_node({var, mem, value, operation});
				L2_node *oper = new L2_node({operation});
				oper->nodes.push_back(src1);
				oper->nodes.push_back(src2);
				root->nodes.push_back(dest);
				root->nodes.push_back(oper);
			}
			L2_arith_tile* clone(){
				return new L2_arith_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				L2_node *d = root->nodes[0];
				L2_node *op = root->nodes[1];
				L2_node *s1 = op->nodes[0];
				L2_node *s2 = op->nodes[1];
				instructs.push_back(new L2_Assignment(d->value, s1->value));	
				instructs.push_back(new L2_Arithmetic(d->value, s2->value, op->value));
			}
	};
	class L2_ret_tile : public  L2_tile {
		public:
			L2_ret_tile(){
				root = new L2_node({ret_type});
				instructs.push_back(new L2_Ret());
			}
			L2_ret_tile* clone(){
				return new L2_ret_tile(*this);
			}
	};

	class L2_ret_var_tile : public  L2_tile {
		public:
			L2_ret_var_tile(){
				root = new L2_node({ret_type});
				L2_node *ret_val = new L2_node({value, var, operation});
				root->nodes.push_back(ret_val);
				
			}
			L2_ret_var_tile* clone(){
				return new L2_ret_var_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				L2_node *var = root->nodes[0];
				Item i;
				i.str = "rax";
				i.t = reg;
				instructs.push_back(new L2_Assignment(i, root->nodes[0]->value));
				instructs.push_back(new L2_Ret());
			}
	};
	class L2_compare_tile : public L2_tile{
		public: 
			L2_compare_tile(){
				root = new L2_node({arrow});
				L2_node *dest = new L2_node({var, mem}); 
				L2_node *src1 = new L2_node({var, mem,  value, operation}); 
				L2_node *src2 = new L2_node({var, mem,  value, operation}); 
				L2_node *oper = new L2_node({compare_op}); 
				oper->nodes.push_back(src1);
				oper->nodes.push_back(src2);
				root->nodes.push_back(dest);
				root->nodes.push_back(oper);
			}
			L2_compare_tile* clone(){
				return new L2_compare_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				L2_node *d = root->nodes[0];
				L2_node *op = root->nodes[1];
				L2_node *s1 = op->nodes[0];
				L2_node *s2 = op->nodes[1];
				instructs.push_back(new L2_Compare(s1->value, s2->value, d->value, op->value));	
			}
	};

	class L2_cond_jump_tile : public  L2_tile {
		public: 
			L2_cond_jump_tile(){
				root = new L2_node({g2});
				L2_node *condNode = new L2_node({var, operation}); 
				L2_node *L1 = new L2_node({label}); 
				L2_node *L2 = new L2_node({label}); 
				root->nodes.push_back(condNode);
				root->nodes.push_back(L1);
				root->nodes.push_back(L2);
			}
			L2_cond_jump_tile* clone(){
				return new L2_cond_jump_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				Item c;
				c.t = operation;
				c.str = "=";
				Item src2;
				src2.t = value;
				src2.val = 1;
				instructs.push_back(new L2_cJump(root->nodes[0]->value, src2, root->nodes[1]->value, root->nodes[2]->value, c));
			}
	};
	class L2_call_tile : public  L2_tile {
		public: 
			L2_call_tile(){
				root = new L2_node({call_type});
				L2_node *fn = new L2_node({var, fn_type, label, mem}); 
				L2_node *args = new L2_node({arg_type}); 
				root->nodes.push_back(fn);
				root->nodes.push_back(args);
			}
			L2_call_tile* clone(){
				return new L2_call_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				vector<string> arg_reg = {"rdi", "rsi", "rdx", "rcx", "r8", "r9"};	
				vector<L2_node*> arguments = root->nodes[1]->nodes;
				int len = arguments.size();
				cout << "length is: " << len << endl;
				for (int i=0; i<len; i++){
					if (i > 6){ // push to stack
						Item dst;
						dst.t = stackArg;
						dst.val = i*8;
						instructs.push_back(new L2_Assignment(dst, arguments[i]->value));
					}else{ // save to reg
						Item dst;
						dst.t = reg;
						dst.str = arg_reg[i];
						instructs.push_back(new L2_Assignment(dst, arguments[i]->value));
					}
				}
				instructs.push_back(new L2_Call_inst(root->nodes[0]->value, len));
			}
	};
	
	class L2_call_ret_tile : public  L2_tile {
		public: 
			L2_call_ret_tile(){
				root = new L2_node({call_type});
				L2_node *dst = new L2_node({var, mem}); 
				L2_node *fn = new L2_node({fn_type, var, label, mem}); 
				L2_node *args = new L2_node({arg_type});
				root->nodes.push_back(dst);
				root->nodes.push_back(fn);
				root->nodes.push_back(args);
			}
			L2_call_ret_tile* clone(){
				return new L2_call_ret_tile(*this);
			}
			void save(map<string,string>* label_names, int* index){
				vector<string> arg_reg = {"rdi", "rsi", "rdx", "rcx", "r8", "r9"};	
				vector<L2_node*> arguments = root->nodes[2]->nodes;
				int len = arguments.size();
				for (int i=0; i<len; i++){
					if (i > 6){ // push to stack
						Item dst;
						dst.t = stackArg;
						dst.val = i*8;
						instructs.push_back(new L2_Assignment(dst, arguments[i]->value));
					}else{ // save to reg
						Item dst;
						dst.t = reg;
						dst.str = arg_reg[i];
						instructs.push_back(new L2_Assignment(dst, arguments[i]->value));
					}
				}
				instructs.push_back(new L2_Call_inst(root->nodes[1]->value, len));
				Item rax;
				rax.t = reg;
				rax.str = "rax";
				instructs.push_back(new L2_Assignment(root->nodes[0]->value, rax)); 
			}
	};
}
