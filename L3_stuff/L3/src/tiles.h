//#include <classes.h>
#include <L3.h>

using namespace std;

namespace L3 {
	void generate_L2_tiles();
	void matchTiles(map<string, string>*, int*, Function*);
	vector<L2_node> coverWithL2 (L3_node, int*, map<string, string>*);
	L3_node* gen_L3_node (vector<Instruction*>);
}
