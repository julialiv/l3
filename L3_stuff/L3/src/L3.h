#pragma once

#include <vector>
#include <classes.h>
#include <L2_classes.h>

namespace L3 {

  struct L3_item {
    std::string labelName;
  };

  struct Function{
    std::string name;
    std::vector<Item*> arguments;
    std::vector<Instruction *> lines;
    std::vector<L2_tile*> L2_nodes;
};


  struct Program{
    //std::string entryPointLabel;
    std::vector<L3::Function *> functions;
  };

} // L5
