#include <string>
#include <iostream>
#include <fstream>
#include <set>
#include <tiles.h>
#include <L2_classes.h>
#include <queue>
#include <typeinfo>
using namespace std;

namespace L3{

	vector<L2_tile*> L2_tiles;

	void generate_L2_tiles() {
		// TODO: MAKE constructor in Ln_Label input bool for whether it's a br or not
		
		// make g2 -> label
		L2_br_label_tile* brt = new L2_br_label_tile();
		L2_tiles.push_back(brt);
		
		// make label
		L2_label_tile* lt = new L2_label_tile();
		L2_tiles.push_back(lt);
		
		// arithmetic
		L2_arith_tile* ar = new L2_arith_tile();
		L2_tiles.push_back(ar);
		
		// return with dest
		L2_ret_var_tile* rv = new L2_ret_var_tile();
		L2_tiles.push_back(rv);
		
		// return
		L2_ret_tile* r = new L2_ret_tile();
		L2_tiles.push_back(r);

		// compare
		L2_compare_tile* c = new L2_compare_tile();
		L2_tiles.push_back(c);
		
		// conditional branches
		L2_cond_jump_tile* cb = new L2_cond_jump_tile();
		L2_tiles.push_back(cb);

		// call with return value 
		L2_call_ret_tile* cr = new L2_call_ret_tile();
		L2_tiles.push_back(cr);
		
		// call
		L2_call_tile* cll = new L2_call_tile();
		L2_tiles.push_back(cll);
		
		// make elem <- elem 
		L2_assign_tile* as = new L2_assign_tile();
		L2_tiles.push_back(as);
	}

	// if we are not able to cover some part of the tree, abort
	vector<L2_tile*> coverWithL2 (L3_node* n3, vector<L2_tile*> L2_tile_list,int* index, map<string, string>* label_names) {
		for (L2_tile* n2 : L2_tiles) {
			// define queues and temp pointers to point to current L3 and L2 elements
			queue<L2_node*> L2_nodes;
			queue<L3_node*> L3_nodes;
			vector<L3_node*> L3_leaves;
			L2_node *L2_ptr;
			L3_node *L3_ptr;
			
			// start by pushing the roots of each tree onto the queue
			L2_nodes.push(n2->root);
			L3_nodes.push(n3);
			
			bool foundMatch = true;
			while (!L2_nodes.empty()) {

				// get the first element on the queue
				L2_ptr = L2_nodes.front();
				L2_nodes.pop();
				L3_ptr = L3_nodes.front();
				L3_nodes.pop();

				// check if the L3 type is viable given the set of possible types (described in L2 node)
				bool typeMatch = false;
				for (Type typ : L2_ptr->types){
					if ((L3_ptr->value).t == typ) {
						typeMatch = true;
						L2_ptr->value = L3_ptr->value;
						if (L3_ptr->value.t == arg_type) {
							// if an arg type, save the nodes vector too
							cout << "length of L3_ptr nodes: " << L3_ptr->nodes.size() << endl;
							L2_ptr->nodes = {};
							for(L3_node* n : L3_ptr->nodes){
								L2_node* new_node = new L2_node(n->value);
								L2_ptr->nodes.push_back(new_node);
							}
							cout << "length of L2_ptr nodes: " << L2_ptr->nodes.size() << endl;
						}
							break;
					}
				}
				if (!typeMatch) {
					foundMatch = false;
					break; // stop checking thie L2 tile cause it doesn't work
				}
			
				// add all children to the queue unless we have an arg_type
				if (L3_ptr->value.t != arg_type) {
					if (L2_ptr->nodes.empty() && L3_ptr->value.t == operation) {
						L3_leaves.push_back(L3_ptr);
					}
					else {
						for (int i = 0; i < L2_ptr->nodes.size(); i++) {
							L2_nodes.push(L2_ptr->nodes[i]);
						}
						for (int j = 0; j < L3_ptr->nodes.size(); j++) {
							L3_nodes.push(L3_ptr->nodes[j]);
						}
					}
				} 
			}
			// taking the first match found, so we need to sort tiles biggest -> smallest
			if (foundMatch) {
				// we found a matching tile, so now we need to somehow mark each L3 element as used??
				// but not remove it, so that we can overlap tiles
				// recursively call this function on each of the children
				L2_tile *new_copy = n2->clone();
				L2_tile_list.push_back(new_copy);
				new_copy->save(label_names, index);
				//for (L3_node* l3 : L3_leaves) {
				//	vector <L2_tile*> newL = coverWithL2(l3, L2_tile_list);
				//	L2_tile_list.insert(L2_tile_list.end(), newL.begin(), newL.end());		
				//} 	
				break;	
			}
		}
		return L2_tile_list;	
	}

	void matchTiles(map<string, string>* label_names, int* index, Function* f) {
		// Create an L3 node for each instruction line in the function
		vector<L3_node*> L3_nodes;
		for (Instruction* i : f->lines) {
			L3_node* n = i->convertToTree();
			L3_nodes.push_back(n);
		}
		// merging would go here
		
		// translate to L2 by finding L2 nodes that "cover" the L3 node well
		// Insert L2 nodes for a given line into growing vector of L2 nodes for the function
		
		for (L3_node* n : L3_nodes) {
			vector<L2_tile*> newTiles;
			newTiles = coverWithL2(n, newTiles, index, label_names);
			//sort tiles here bottom up, need tree
			(f->L2_nodes).insert((f->L2_nodes).end(), newTiles.begin(), newTiles.end());
		}
		
	}	
}
