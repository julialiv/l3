#include <spill.h>
#include <string>
#include <iostream>

using namespace std;

namespace L2 {

 void printFunction(Function* f){
   cout << "(" << f->name << endl;
   cout << "\t" << f->arguments << " " << f->locals << endl;
   for (int i = 0; i < (f->lines).size(); i = i+1) {
     	Instruction* instruct = f->lines[i];
    	cout << "\t" << f->lines[i]->print() << endl;
   }
   cout << ")" << endl;
 }

 // Load: s0 <- mem rsp 0
 // Store: mem rsp 0 <- s0
 void gen_loadstore(int locs, vector<Instruction*>* newLines, bool load, string newVar, int varNum){
	Assignment* as = new Assignment();
  as->type = LineType::Assign;
	Item memItem;
	memItem.t=Type::mem;
	memItem.str = "rsp";
  	memItem.val = locs*8;// + 8*varNum;

	Item varItem;
	varItem.t=Type::var;
	varItem.str = newVar;

	if (load){
		as->src = memItem;
		as->dst = varItem;
	} else {
		as->src = varItem;
		as->dst = memItem;
	}
	// Add the load or store instruction into our program p
	newLines->push_back(as);
 }

 bool loadStore(int locs, vector<Instruction*> *newLines, bool load, bool store, string newVar, Instruction *in, int varNum) {
   if (!load && !store) {
     newLines->push_back(in);
     return false;
   } else if (load && store) {
     gen_loadstore(locs, newLines, true, newVar, varNum);
     newLines->push_back(in);
     gen_loadstore(locs, newLines, false, newVar, varNum);
     return true;
   } else if (load) {
     gen_loadstore(locs, newLines, true, newVar, varNum);
     newLines->push_back(in);
     return true;
   } else {
     newLines->push_back(in);
     gen_loadstore(locs, newLines, false, newVar, varNum);
     return true;
   }
 }


 // Go thru and replace all instances of toSpill with spillVar + index
 // If the variable is read: add a load loadOnly
 // If the variable is written: add a store storeOnly
 // Increment the local variable number to 1 if you spill?
Function* REG_spill(Program p, Function* oldF, string toSpill, string newVar){
	int var_index = 0;
	//string toSpill = to_spill.str;
	//string newVar = new_var.str;

  // copy old function into new one
 // Function *newFunc;
  //newFunc->arguments = oldF->arguments;
  //newFunc->name = oldF->name;
  //newFunc->locals = oldF->locals;
  vector<Instruction*> newLines = vector<Instruction*>();

  // copy lines of old fn into new one, adding loads and stores when necessary
	for (int i = 0; i < (oldF->lines).size(); i=i+1) {
	    Instruction* instruct = oldF->lines[i];
	    string tmpVar = newVar + to_string(var_index);
	    bool store = false;
	    bool load = false;

		switch(instruct->type){
			case(Assign):{
				Assignment* as = static_cast<Assignment*>(instruct);
				// Read: add load
				if (toSpill.compare(as->src.str) == 0){
					as->src.str = tmpVar;
          				load = true;
				}
				// Written: add store
				if (toSpill.compare(as->dst.str) == 0){
					
					as->dst.str = tmpVar;
					if (as->dst.t == mem)
						load = true;
          				else
						store = true;
				}
				break;
			}
			case(Arith): {
        Arithmetic* ar = static_cast<Arithmetic*>(instruct);

        if (toSpill.compare(ar->dst.str) == 0){
          ar->dst.str = tmpVar;
          load = true;
          store = true;
        }
        if (toSpill.compare(ar->src.str) == 0){
					ar->src.str = tmpVar;
          load = true;
				}
				break;
      }
			case(decremIncrem): {
        DI* di = static_cast<DI*>(instruct);
        if (toSpill.compare(di->dst.str) == 0){
					di->dst.str = tmpVar;
          load = true;
          store = true;
				}
				break;
      }
			case(Comparison): {
        Compare* cmp = static_cast<Compare*>(instruct);

        if (toSpill.compare(cmp->src1.str) == 0){
					cmp->src1.str = tmpVar;
          load = true;
				}
        if (toSpill.compare(cmp->src2.str) == 0){
					cmp->src2.str = tmpVar;
          load = true;
				}
        if (toSpill.compare(cmp->dst.str) == 0){
					cmp->dst.str = tmpVar;
          load = true;
          store = true;
				}
				break;
      }
			case(conditional_jump): {
        cJump* cj = static_cast<cJump*>(instruct);
        if (toSpill.compare(cj->src1.str) == 0){
					cj->src1.str = tmpVar;
          load = true;
				}
        if (toSpill.compare(cj->src2.str) == 0){
					cj->src2.str = tmpVar;
          load = true;
				}
				break;
      }
			case(call_line): {
        Call_inst* call = static_cast<Call_inst*>(instruct);
        if (toSpill.compare(call->fn.str) == 0){
					call->fn.str = tmpVar;
          load = true;
				}
				break;
      }
			case(leaType): {
				LEA* lea = static_cast<LEA*>(instruct);
				if (toSpill.compare(lea->dst.str) == 0){
					lea->dst.str = tmpVar;
					  store = true;
				}
				if (toSpill.compare(lea->src1.str) == 0){
					lea->src1.str = tmpVar;
					  load = true;
				}
				if (toSpill.compare(lea->src2.str) == 0){
					lea->src2.str = tmpVar;
					  load = true;
				}
				break;
		  }
		  default:
  			break;
		}
	    	bool usedVar = loadStore(oldF->locals, &newLines, load, store, tmpVar, instruct, var_index);
    		if (usedVar) var_index++;
	}
	if (var_index > 0)
		oldF->locals = oldF->locals + 1;
	oldF->lines = newLines; 
	//delete (oldF);
	return oldF;
	//printFunction(newFunc);
  }
}
