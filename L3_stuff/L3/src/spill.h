#pragma once

#include <classes.h>
#include <L2.h>


namespace L2 {
	void gen_loadstore(Function*, bool, string, int, int);
	Function* REG_spill(Program, Function*, string, string);
	void printFunction(Function* );
}
