#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <set>
#include <iterator>
#include <cstring>
#include <cctype>
#include <cstdlib>
#include <stdint.h>
#include <unistd.h>
#include <iostream>
#include <parser.h>
#include <code_generator.h>

using namespace std;

void print_help (char *progName){
  std::cerr << "Usage: " << progName << " [-v] [-g 0|1] [-O 0|1|2] [-s] [-l 1|2] SOURCE" << std::endl;
  return ;
}

int main(int argc, char **argv){

  bool enable_code_generator = true;
  int32_t liveness_only = false;
  int32_t optLevel = 0;
  bool interference_only = false;

  /* Check the input.
   */
  //Utils::verbose = false;
  if( argc < 2 ) {
    print_help(argv[0]);
    return 1;
  }
  int32_t opt;
  while ((opt = getopt(argc, argv, "vig:O:sl:")) != -1) {
    switch (opt){
      case 'O':
        optLevel = strtoul(optarg, NULL, 0);
        break ;

      case 'g':
        enable_code_generator = (strtoul(optarg, NULL, 0) == 0) ? false : true ;
        break ;
      case 'v':
      //TODO
        break ;
      default:
        print_help(argv[0]);
        return 1;
    }
  }

  /*
   * Parse the input file.
   */
    L3::Program p;

    /*
     * Parse the L3 program.
     */
    p = L3::parse_file(argv[optind]);
   cout << "parsed file" << endl;
  /*
   * Special cases.
   */

  /*
   * Generate the code.
   */
  if (enable_code_generator){
    cout <<"generating code asdf" << endl;
    L3::L3_generate_code(p);
    cout <<"generated code" << endl;
  }

  return 0;
}
