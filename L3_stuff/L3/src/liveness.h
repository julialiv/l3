#pragma once

#include <L2.h>
#include <classes.h>

namespace L2{
  struct genKill;

  DataFlowResult* computeLivenessAnalysis(Program p, Function *f);
  void computeGenKill (Program p, Function *f);
  void setGen(Item src, int i);
  void setKill(Item dst, int i);
  string toAString(DataFlowResult*);
}
