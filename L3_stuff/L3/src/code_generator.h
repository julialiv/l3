#pragma once

#include <L3.h>

namespace L3{

  void L3_generate_code(Program);
  void printF(Function *, ofstream* );
  void printL2(Program);
}
