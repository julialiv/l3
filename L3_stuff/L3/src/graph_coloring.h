#pragma once


#include <classes.h>
#include <L2.h>


namespace L2 {
	void removeNode(node, map<node, set<node>>* );
	bool sortByNumEdges(pair<node, int>, pair<node, int>);
	node selectNode(map<node, set<node>> );
	map<node, set<node>> color_graph(int, DataFlowResult*, Program, Function*);
	map<node, set<node>> recompute(int, Function *, Program );
	void addNodeBack(node*, set<node>, map<node, set<node>>* );
	string findColor(node, set<node>, map<node, set<node>>*);
	void setColor (node*, string, set<node>, map<node, set<node>>*);
	void printSuccessors(Function *f);
}
