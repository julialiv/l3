//each instruction holds a set of possible successors

#include <successors.h>
#include <map>
#include <iostream>

using namespace std;

// Store index of the successor(s) line
// Switch on instruction type
// Successors

namespace L2 {
  // Connect Ln_Labels to their corresponding Instruction indices for use in computeSuccessors
  std::map<string,int> label2index (Program p, Function *f) {
	std::map<string,int> indexLabels;
	for (int i = 0; i < f->lines.size(); i = i+1) {
    		// Only process line labels
		Instruction * line = f->lines[i];
		if(f->lines[i]->type == lineLabel){
			Ln_Label* line_label = static_cast<Ln_Label*>(line);
			if (!line_label->goToFlag)
				indexLabels[line_label->lab] = i;
		}
  	}
	return indexLabels;
  }

  void computeSuccessors (Program p, Function *f) {
      //cout << "entered compute successors" << endl;
      std::map<string,int> labelIndices = label2index(p, f);

      int count = 0;
    	// For each instruction, compute and save successors
      for (auto instruct : f->lines) {
         //cout << "instruction #" << count << endl;
	instruct->successor_set = set<int>();  
    	 switch(instruct->type){
              //cout << "instruction type" << instruct->type << endl;
              case conditional_jump: {
                //cout << "in conditional_jump" << endl;
                cJump* cj = static_cast<cJump*>(instruct);
                int l1 = labelIndices[cj->label1.str];
                int l2 = labelIndices[cj->label2.str];
                cj->successor_set.insert(l1);
                cj->successor_set.insert(l2);
                break;
              }

              case lineLabel: {
                //cout << "in line label" << endl;
                Ln_Label* l = static_cast<Ln_Label*>(instruct);
                if (l->goToFlag){
                    int l1 = labelIndices[l->lab];
                    l->successor_set.insert(l1);
                }
                else {
                    instruct->successor_set.insert(count+1);
                }
                break;
              }
              case Return: {
                //cout << "return" << endl;
                instruct->successor_set = {};
                break;
              }
              default:
                //cout << "just adding next line" << endl;
                instruct->successor_set.insert(count+1);
                break;
            }
          count++;
      }
  }
}
