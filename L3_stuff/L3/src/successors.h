#pragma once

#include <L2.h>
#include <map>

namespace L2{

  void computeSuccessors (Program, Function*);
  std::map<string,int> label2index (Program, Function*);

}
