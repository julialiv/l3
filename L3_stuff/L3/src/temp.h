#pragma once

using namespace std;

namespace L3 {
	class Item;
	struct L3_node;
	class Instruction;
	struct L2_node;
	class Ln_Label;
	class Ret;
	class Assignment;
	class Compare;
	class cJump;
	class Arithmetic;
	class Call_inst;
}
