#pragma once

#include <vector>
#include <classes.h>

namespace L2 {

  struct L2_item {
    std::string labelName;
  };

  struct Function{
    std::string name;
    std::vector<Item*> arguments;
    std::vector<Instruction *> lines;
  };


  struct Program{
    //std::string entryPointLabel;
    std::vector<L2::Function *> functions;
    spill_structure spill;
  };

} // L5
