#include <graph_coloring.h>
#include <successors.h>
#include <liveness.h>
#include <interference.h>
#include <spill.h>
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <functional>
#include <algorithm>

using namespace std;

namespace L2 {

  // order matters
  string colors [16] = {"rdi", "rsi", "rdx", "r11", "r8", "r9", "r10", "rdx", "rax" , "r14", "r13", "r12", "r15", "rbp", "rbx", "rcx"};
  char alpha = 'a';

  // matching indices for both vectors
  vector<node> stack_key;
  vector<set<node>> stack_edges;
  vector<node> nodes2spill;


  void removeNode(node targetNode, map<node, set<node>>* graph) {
    set<node> edges = graph->at(targetNode);
    graph->erase(targetNode);
    stack_key.push_back(targetNode);
    stack_edges.push_back(edges);
  }

  bool sortByNumEdges(pair<node, int> n1, pair<node, int> n2) {
    return n1.second > n2.second;
  };

  void sortNodes(map<node, set<node>>* graph, vector<node>* sortLess, vector<node>* sortMore) {
    vector<pair<node, int>> setOfNodes;
    for (std::map<node, set<node>>::iterator it=graph->begin(); it!=graph->end(); ++it)
    {
      setOfNodes.push_back(make_pair(it->first, it->second.size()));
    }

    sort(setOfNodes.begin(), setOfNodes.end(), sortByNumEdges);
    // Iterate over a set using range base for loop
    // It will display the items in sorted order of values
    //cout << "set of nodes len: " << setOfNodes.size() << endl;
    int index = 0;
    for (pair<node, int> element : setOfNodes) {
      if (element.second < 15) {
        sortLess->push_back(element.first);
      }
      else {
        sortMore->push_back(element.first);
      }
    }
  }

  void printSuccessors(Function *f) {
    int i = 0;
    for (auto instruct : f->lines) {
      cout << "succesors for line #" << i << " are: ";
      for (int successorIndex : instruct->successor_set)
      cout << successorIndex << " ";
      cout << endl;
      i++;
    }
  }

  map<node, set<node>> recompute(int count, Function *f, Program p) {
    computeSuccessors (p, f);
    //printSuccessors(f);
    DataFlowResult* df = computeLivenessAnalysis(p, f);
    compute_interference(df, p, f);
    //printGraph(df);
    map<node, set<node>> colored_graph = color_graph(count, df, p, f);
    delete(df);
    return colored_graph;
  }

  string findColor(node n, set<node> orig_edges, map<node, set<node>>* graph) {

    set<node> edges = graph->at(n);

    for (auto c : colors){
      bool foundInEdges = false;
      for (node edge_color : edges) {
        if (c == edge_color.color) {
          foundInEdges = true;
	  break;
	}
      }
      for (node e : orig_edges) {
        if (e.isReg && c == e.color) {
            foundInEdges = true;
	    break;
	  }
      }
      if (!foundInEdges ) {
     //   cout << "CHOSEN COLOR " << c << endl;
        return c;
      }
    }
    //cout << "NO color found" <<endl;
    return "";
  }

  void addNodeBack(node *n, set<node> edges, map<node, set<node>>* graph) {
    // Insert the node and its edges back into the graph
    set<node> edgesToAdd = set<node>();
    for (auto it : *graph) {
      node key = it.first;
      //cout << "key's color is " << it.first.color << endl;
      for (node e : edges) {
        if (key == e) {
            graph->at(key).insert(*n);
	  //  cout << "node's color is: " << n->color << endl;
	    //cout << "key in graph already's color is " << key.color << endl;
	   // cout << "adding edge between " << key.name << " and " << n->name << " with color " << key.color << endl;
	    edgesToAdd.insert(key);
        }
      }
    }
    //cout << "adding node " << n->name << endl;
    graph->insert(pair<node, set<node>>(*n, edgesToAdd));
  }

  void setColor (node *n, string color, set<node> edges, map<node, set<node>>*graph) {
    node colored = {n->name, false, color, false};
    set<node> edgesNew = graph->at(*n);
    graph->erase(*n);
    graph->insert(pair<node, set<node>>(colored, edgesNew));
    // Update all references to this node as we add it back
    
    
    for (auto it : *graph) {
      for (auto e : it.second) {
        if (!(e.name).compare(n->name)) {
   	  // cout << "updating color of " << e.name << " to " << color << endl;
	   e.color = color;
	}
      }
    }
  }

  map<node, set<node>> color_graph(int count, DataFlowResult* dataFlow, Program p, Function *f) {
    map<node, set<node>>* graph = dataFlow->i_graph;
    vector<node>* sortedNodesLess = new vector<node>();
    vector<node>* sortedNodesGreater = new vector<node> ();

    // Sort all nodes in the graph so it is easy to remove them
    sortNodes(graph, sortedNodesLess, sortedNodesGreater);

    // repeatedly select and remove nodes until graph is empty
    while (graph->size() != 0) {
      if (sortedNodesGreater->size() != 0){
        node n2 = sortedNodesGreater->back();
//	cout << "removing node (g) " << n2.name << endl; 
        removeNode(n2, graph);
        sortedNodesGreater->pop_back();
      }
      else if (sortedNodesLess->size() != 0) {
        node n1 = sortedNodesLess->back();
//	cout << "removing node (l) " << n1.name << endl; 
        removeNode(n1, graph);
        sortedNodesLess->pop_back();
      }
      else {
        cout << "ERROR: graph is empty" << endl;
        break;
      }
    }

    // rebuild graph
    while (stack_key.size() != 0) {
      node n = stack_key.back();
      set<node> edges = stack_edges.back();
      stack_key.pop_back();
      stack_edges.pop_back();
      addNodeBack(&n, edges, graph);
	
      if (!n.isReg){
        string color = findColor(n, edges, graph);
        if (color.empty()) {
          //cout << "color is: " << color << endl;
         // cout << "We want to spill " << n.name << endl;
          //n.spilled = true;
          nodes2spill.push_back(n);
        }
        else {
          setColor(&n, color, edges, graph);
	  for (auto it : *graph) {
	      node key = it.first;
//	      cout << "key " << key.name << "'s color is " << it.first.color << endl;
	  }
	}
      }

    }
    // maybe cleaner to have a wrapper funtion that calls
    // liveness, interference, and graph coloring
    bool hadToSpill = false;
   // cout << nodes2spill.size() << endl;
    while(nodes2spill.size() != 0) {
     // cout << nodes2spill.size() << endl;
      node n = nodes2spill.back();
      string strAlpha = string(1,alpha);
      f = REG_spill(p, f, n.name, strAlpha); // sketchly alphabet usage
     // cout << "SPILLED: " << n.name << endl;
      alpha += 1;
      hadToSpill = true;
      nodes2spill.pop_back();
    //  cout << "popped" << endl;
    }

    delete (sortedNodesLess);
    delete (sortedNodesGreater);
    if (!hadToSpill) {
	printGraph(dataFlow);
    	return *dataFlow->i_graph;
    }
    if (count > 10 ) {
   //	cout << " INFINITE LOOP " << endl;
        return *dataFlow->i_graph;
    }
    else
    	return recompute(count+1, f, p);
  }
}
