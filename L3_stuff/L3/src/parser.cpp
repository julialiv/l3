#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <set>
#include <iterator>
#include <cstring>
#include <cctype>
#include <cstdlib>
#include <stdint.h>
#include <assert.h>
#include <iostream>

#include <parser.h>
//#include <classes.h>

#include <tao/pegtl.hpp>
#include <tao/pegtl/analyze.hpp>
#include <tao/pegtl/contrib/raw_string.hpp>

namespace pegtl = tao::TAO_PEGTL_NAMESPACE;

using namespace pegtl;
using namespace std;

namespace L3 {

  /*
   * Data required to parse
   */
	// params do not go on this list
  std::vector< Item> parsed_registers;
  std::vector<Call_inst*> call_lines;
  /*
   * Grammar rules from now on.
   */


  struct comment:
    pegtl::disable<
      TAOCPP_PEGTL_STRING( "//" ),
      pegtl::until< pegtl::eolf >
    > {};

  struct seps:
    pegtl::star<
      pegtl::sor<
        pegtl::ascii::space,
        comment
      >
    > {};

  struct label_rule:
    pegtl::seq<
      pegtl::one<':'>,
      pegtl::plus<
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >
        >
      >,
      pegtl::star<
        pegtl::sor<
          pegtl::alpha,
          pegtl::one< '_' >,
          pegtl::digit
        >
      >
    > {};

 // aop includes sop
 struct aop:
	pegtl::sor<
		pegtl::one<'+'>,
		pegtl::one<'-'>,
		pegtl::one<'*'>,
		pegtl::one<'&'>,
		TAOCPP_PEGTL_STRING( ">>" ),
		TAOCPP_PEGTL_STRING( "<<" )
	> {};

struct cmp:
	pegtl::sor<
		TAOCPP_PEGTL_STRING( "<=" ),
		TAOCPP_PEGTL_STRING( "<" ),
		TAOCPP_PEGTL_STRING( "=" ),
		TAOCPP_PEGTL_STRING( ">" ),
		TAOCPP_PEGTL_STRING( ">=" )
	> {};

 struct L3_label_rule:
    label_rule {};


 struct br:
	TAOCPP_PEGTL_STRING( "br" ) {};

struct line_label:
	pegtl::seq<
		seps,
		pegtl::opt<
			br
		>,
		seps,
		L3_label_rule,
		seps
	> {};

struct word:
  pegtl::seq<
    pegtl::plus<
      pegtl::sor<
        pegtl::alpha,
        pegtl::one< '_' >
      >
    >,
    pegtl::star<
      pegtl::sor<
        pegtl::alpha,
        pegtl::one< '_' >,
        pegtl::one< '-' >,
        pegtl::digit
      >
    >
  > {};

struct run_time_fn:
  pegtl::sor<
    TAOCPP_PEGTL_STRING( "print" ),
    TAOCPP_PEGTL_STRING( "reset_heap" ),
    TAOCPP_PEGTL_STRING( "alloc_heap" ),
    TAOCPP_PEGTL_STRING( "switch_heaps" ),
    TAOCPP_PEGTL_STRING( "gc" ),
    TAOCPP_PEGTL_STRING( "allocate" ),
    TAOCPP_PEGTL_STRING( "array-error" ),
    TAOCPP_PEGTL_STRING( "array_error" )
  > {};

struct variable:
  word {} ;


  struct function_name:
    label_rule {};

  struct number:
    pegtl::seq<
      pegtl::opt<
        pegtl::sor<
          pegtl::one< '-' >,
          pegtl::one< '+' >
        >
      >,
      pegtl::plus<
        pegtl::digit
      >
    >{};

  struct value_rule:
	pegtl::seq<
		seps,
		number,
		seps
	>{} ;

  struct mem_rule:
  	pegtl::seq<
  		seps,
		pegtl::sor<
  			TAOCPP_PEGTL_STRING( "load" ),
  			TAOCPP_PEGTL_STRING( "store" )
		>,
  		seps,
		variable,
  		seps
  	>{};

  struct dest_rule:
  	pegtl::sor<
  		mem_rule,
      		variable
    	>{};

  struct t:
	pegtl::sor<
		variable,
		value_rule
	>{};

  struct src_rule:
  	pegtl::seq<
  		seps,
  		pegtl::sor<
  			mem_rule,
  			L3_label_rule,
        		t
  		>,
  		seps
  	>{};

 struct ret_var:
	pegtl::seq<
  		seps,
  		TAOCPP_PEGTL_STRING( "return" ),
		seps,
		t,
		seps
	>{};

  struct ret:
  	pegtl::seq<
  		seps,
  		TAOCPP_PEGTL_STRING( "return" ),
		seps
  	> {};

  struct cond_jump:
  	pegtl::seq<
  		seps,
  		TAOCPP_PEGTL_STRING( "br" ),
  		seps,
  		variable,
  		seps,
  		L3_label_rule,
  		seps,
  		L3_label_rule,
  		seps
  	> {};

  struct comparison_rule:
	pegtl::seq<
		seps,
		variable,
		seps,
		TAOCPP_PEGTL_STRING( "<-" ),
		seps,
		t,
		seps,
		cmp,
		seps,
		t,
		seps
	> {};

  // ex: rax <- rdi
  struct assignment_rule:
      pegtl::seq<
      	seps,
      	dest_rule,
      	seps,
      	TAOCPP_PEGTL_STRING("<-"),
      	seps,
      	src_rule,
      	seps
      >{};


 // arithmetic rule expanded to include sop
  struct arithmetic_rule:
    pegtl::seq<
    	seps,
    	variable,
	seps,
      	TAOCPP_PEGTL_STRING("<-"),
 	seps,
	t,
	seps,		
    	aop,
    	seps,
    	t,
    	seps
    >{};

  // So we can initialize a list of call arguments
  struct call_keyword:
	TAOCPP_PEGTL_STRING("call"){};
	
  struct call_args:
	pegtl::sor<
		value_rule,
		variable
	>{};

  struct multipleArgs:
	pegtl::seq<
		call_args,
		seps,
		pegtl::one<','>
	>{};

  struct call_store:
	pegtl::seq<
		seps,
		variable,
		seps,
		TAOCPP_PEGTL_STRING( "<-" ),
		seps,
		call_keyword,
		seps,
		pegtl::sor<
			L3_label_rule,
      			run_time_fn,
      			variable
		>,
		seps,
		pegtl::one<'('>,
		seps,
		pegtl::star<
			pegtl::sor<
    				pegtl::seq<pegtl::at<multipleArgs>, multipleArgs>,
				call_args
			>,
			seps
		>,
		pegtl::one<')'>,
		seps
	> {};

  struct call:
	pegtl::seq<
		seps,
		call_keyword,
		seps,
		pegtl::sor<
			L3_label_rule,
      			run_time_fn,
      			variable
		>,
		seps,
		pegtl::one<'('>,
		seps,
		pegtl::star<
			pegtl::sor<
    				pegtl::seq<pegtl::at<multipleArgs>, multipleArgs>,
				call_args
			>,
			seps
		>,
		pegtl::one<')'>,
		seps
	> {};

  struct body:
    pegtl::star<
	// options here to fall into the correct "rule" for each line
    	pegtl::sor<
    		pegtl::seq<pegtl::at<call_store>, call_store>,
    		pegtl::seq<pegtl::at<comparison_rule>, comparison_rule>,
    		pegtl::seq<pegtl::at<ret_var>, ret_var>,
    		pegtl::seq<pegtl::at<ret>, ret>,
    		pegtl::seq<pegtl::at<line_label>, line_label>,
    		pegtl::seq<pegtl::at<arithmetic_rule>, arithmetic_rule>,
    		pegtl::seq<pegtl::at<assignment_rule>, assignment_rule>,
    		pegtl::seq<pegtl::at<cond_jump>, cond_jump>,
		pegtl::seq<pegtl::at<call>, call>
    	>
    >{};

  struct param:
  	word {} ;

  struct L3_function_rule:
    pegtl::seq<
      seps,
      TAOCPP_PEGTL_STRING("define"),
      seps,
      function_name,
      seps,
      pegtl::one< '(' >,
      seps,
      pegtl::star<
	param,
	seps,
	pegtl::opt<
      		pegtl::one< ',' >,
		seps
	>
      >,
      pegtl::one< ')' >,
      seps,
      pegtl::one< '{' >,
      seps,
      body,
      seps,
      pegtl::one< '}' >,
      seps
    > {};

// assignment rules here

  struct L3_functions_rule:
    pegtl::seq<
      seps,
      pegtl::plus< L3_function_rule >
    > {};

  struct grammar :
    pegtl::must<
      L3_functions_rule
    > {};


  /*
   * Actions attached to grammar rules.
   */
  template< typename Rule >
  struct action : pegtl::nothing< Rule > {};

  //template<> struct action < label_rule > {
   // template< typename Input >
    //static void apply( const Input & in, L3::Program & p){
     // if (p.entryPointLabel.empty()){
     //   p.entryPointLabel = in.string();
   //   }
   // }
  //};

  template<> struct action < param > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item* i = new Item;
      i->str = in.string();
      i->t = L3::Type::var;
      p.functions.back()->arguments.push_back(i);
   }
 };

 template<> struct action < run_time_fn > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item i;
      i.str = in.string();
      i.t = L3::Type::fn_type;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < function_name > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Function *newF = new L3::Function();
      newF->name = in.string();
      p.functions.push_back(newF);
    }
  };

  template<> struct action < L3_label_rule > {
    template< typename Input >
		static void apply( const Input & in, L3::Program & p){
      L3::Item i;
      i.str = in.string();
      i.t = L3::Type::label;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < line_label > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Ln_Label *newL = new L3::Ln_Label();
      newL->type = L3::LineType::lineLabel;
      L3::Item label = parsed_registers.back();
      parsed_registers.pop_back();
      newL->goToFlag = false;
      if (parsed_registers.back().t == g2){
      	parsed_registers.pop_back();
      	newL->goToFlag = true;
      }
      newL->lab = label;
      p.functions.back()->lines.push_back(newL);
//	cout << "completed line_label action"<< endl;
    }
  };

  template<> struct action < br > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item i;
      i.str = in.string();
      i.t = L3::Type::g2;
      parsed_registers.push_back(i);
//	cout << "completed br action"<< endl;
    }
  };

  template<> struct action < ret_var > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Ret *newL = new L3::Ret();
      L3::Item var = parsed_registers.back();
      parsed_registers.pop_back();
      newL->ret_val = var;
      newL->type = L3::LineType::Return;
      newL->flag = true;
      p.functions.back()->lines.push_back(newL);
    }
  };
  template<> struct action < ret > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Ret *newL = new L3::Ret();
      newL->type = L3::LineType::Return;
      newL->flag = false;
      p.functions.back()->lines.push_back(newL);
    }
  };
  
  template<> struct action < call_args > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item arg = parsed_registers.back();
      parsed_registers.pop_back();
      L3::Call_inst *cl = call_lines.back();
      cl->list_args.push_back(arg);
    }
 };
 
  template<> struct action < call_keyword > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Call_inst *newA = new L3::Call_inst();
       call_lines.push_back(newA);
    }
  };

  template<> struct action < call_store > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Item fn = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Call_inst* cl = call_lines.back();
       cl->fn = fn;
       cl->hasDest = true;
       cl->dst = dest;
       cl->type = L3::LineType::call_line;
       cout << "length of list args in parser: " << cl->list_args.size() << endl;
       p.functions.back()->lines.push_back(cl);
       call_lines.pop_back();
    }
  };
  template<> struct action < call > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Item fn = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Call_inst* cl = call_lines.back();
       cl->fn = fn;
       cl->hasDest = false;
       cl->type = L3::LineType::call_line;
       p.functions.back()->lines.push_back(cl);
       call_lines.pop_back();
//	cout << "completed call action"<< endl;
    }
  };

  template<> struct action < assignment_rule > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Assignment *newA = new L3::Assignment();
       L3::Item src = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       newA->dst = dest;
       newA->src = src;
       newA->type = L3::LineType::Assign;
       p.functions.back()->lines.push_back(newA);
//	cout << "completed assignment_rule action"<< endl;
    }
  };


  template<> struct action < cmp > {
    template< typename Input >
		static void apply( const Input & in, L3::Program & p){
      L3::Item i;
      i.str = in.string();
      i.t = L3::Type::compare_op;
      parsed_registers.push_back(i);
    }
  };


  template<> struct action < aop > {
    template< typename Input >
		static void apply( const Input & in, L3::Program & p){
      L3::Item i;
      i.str = in.string();
      i.t = L3::Type::operation;
      parsed_registers.push_back(i);
//	cout << "completed aop action"<< endl;
    }
  };

  template<> struct action < cond_jump > {
   template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::cJump *newC = new L3::cJump();
       L3::Item label2 = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item label1= parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item src = parsed_registers.back();
       parsed_registers.pop_back();
       newC->label1 = label1;
       newC->label2 = label2;
       newC->src = src;
       newC->type = L3::LineType::conditional_jump;
       p.functions.back()->lines.push_back(newC);
//	cout << "completed cond_jump action"<< endl;
    }
  };

  template<> struct action < comparison_rule > {
   template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Compare *newC = new L3::Compare();
       L3::Item src2 = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item comp= parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item src1 = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item dst = parsed_registers.back();
       parsed_registers.pop_back();
       newC->dst = dst;
       newC->src1 = src1;
       newC->src2 = src2;
       newC->c = comp;
       newC->type = L3::LineType::Comparison;
       p.functions.back()->lines.push_back(newC);
    }
  };


  template<> struct action < arithmetic_rule > {
   template< typename Input >
    static void apply( const Input & in, L3::Program & p){
       L3::Arithmetic *newA = new L3::Arithmetic();
       L3::Item src2 = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item op = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item src1 = parsed_registers.back();
       parsed_registers.pop_back();
       L3::Item dest = parsed_registers.back();
       parsed_registers.pop_back();
       newA->dst = dest;
       newA->src1 = src1;
       newA->src2 = src2;
       newA->op = op;
       newA->type = L3::LineType::Arith;
       p.functions.back()->lines.push_back(newA);
//	cout << "completed arithmentic_rule action"<< endl;
    }
  };

  template<> struct action < variable > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item newI;
      newI.str = in.string();
      newI.t = L3::Type::var;
      parsed_registers.push_back(newI);
    }
  };

  template<> struct action <value_rule> {
    template <typename Input>
    static void apply (const Input & in, L3::Program &p) {
      L3::Item newI;
      newI.val = std::stoll(in.string());
      newI.t = L3::Type::value;
      parsed_registers.push_back(newI);
    }
  };

  template<> struct action < mem_rule > {
    template< typename Input >
    static void apply( const Input & in, L3::Program & p){
      L3::Item newI;
      L3::Item var_mem = parsed_registers.back();
      parsed_registers.pop_back();
      newI.str = var_mem.str;
      newI.t = L3::Type::mem;
      parsed_registers.push_back(newI);
    }
  };

  Program parse_file (char *fileName){
    /*
     * Check the grammar for some possible issues.
     */
    pegtl::analyze< L3::grammar >();

    /*
     * Parse.
     */
    file_input< > fileInput(fileName);
    L3::Program p;
    parse< L3::grammar, L3::action >(fileInput, p);
    return p;
  }

} // L3
