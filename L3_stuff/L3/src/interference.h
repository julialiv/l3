#pragma once
#include<map>
#include <L2.h>
#include <classes.h>

namespace L2 {
  void compute_interference(DataFlowResult* , Program, Function*);
  void addNodes(set<string>, map<node, set<node>>);
  void printGraph(DataFlowResult*);
}
