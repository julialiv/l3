#include <string>
#include <iostream>
#include <fstream>
#include <code_generator.h>
#include <tiles.h>

using namespace std;

namespace L3{

  set<string> registerList = {"rdi", "rsi", "r10", "r11", "r8", "r9", "rcx", "rdx", "rax" , "r14", "r13", "r12", "r15", "rbp", "rbx"};	
  
  void printL2(Program p) {
    std::ofstream outputFile;
    outputFile.open("prog.L2");
     
    outputFile << "(:main" << endl;
    
    for (Function *f : p.functions) {
    	printF(f, &outputFile);
    }
    outputFile << ")" << endl;
    outputFile.close();
  }

  void printF(Function *f, ofstream* outputFile) {
    *outputFile << "\t(" << f->name << endl;
    *outputFile << "\t\t" << f->arguments.size() << " 0" << endl;
    int countTile = 1;
    for (L2_tile* t: f->L2_nodes) {
	for (L2_Instruction *i : t->instructs) {
		*outputFile << "\t\t" << i->print() << endl;
	}
	countTile++;
    }
    *outputFile << "\t)" << endl;
  }

  void L3_generate_code(Program p) {
    generate_L2_tiles();
    int index = 0;
    int* ptr = &index;	
    for (auto f : p.functions){
        map<string, string>* label_names = new map<string, string>();
	matchTiles(label_names, ptr, f);
	free(label_names);
    } 
    printL2(p);
    return ;
  }

}
