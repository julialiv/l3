#include <interference.h>
#include <iostream>
#include <typeinfo>

using namespace std;

namespace L2 {

  set<string> registers = {"r10", "r11", "r8", "r9", "rcx", "rdi", "rdx", "rsi",
    "r12", "r13", "r14", "r15", "rbp", "rbx", "rax"};

  set<node> processToNode(set<string> strSet) {
      set<node> nodeSet;
      for (auto str : strSet) {
	      node newN = {str, false, "", false};
        // Flag register nodes
        if (registers.find(str) != registers.end()) {
          newN.isReg = true;
          newN.color = str;
	      }
        nodeSet.insert(newN);
      }
      return nodeSet;
  }

  set<node> register_nodes = processToNode(registers);

  // Create nodes and their edges for interference graph
  map<node, set<node>>* addNodes(set<string> ioSet, map<node, set<node>>* graph) {
    // Convert all strings to nodes for use in graph
    set<node> ioNodes = processToNode(ioSet);
    for (node n : ioNodes) {
        // Node does not yet exist in graph
       if (!graph->count(n)) {
  // In same in or out set, so edges
	  graph->insert(pair<node, set<node>>(n, set<node>()));
	  // All register nodes are connected to all other registers
	  if (n.isReg){
	    graph->at(n).insert(register_nodes.begin(), register_nodes.end());
	  }
	}
	graph->at(n).insert(ioNodes.begin(), ioNodes.end());
        // Remove the connection to itself
        graph->at(n).erase(n);
    }
    return graph;
  }

void connectKillOut(DataFlowResult* df, Function f) {
	for (int i = 0; i < df->globalGenKill.size(); i++) {
		bool shouldConnect = true;
		if (f.lines[i]->type == Assign){
			Assignment* as = static_cast<Assignment*>(f.lines[i]);
			if (as->src.t == var || as->src.t == reg) {
				if (as->dst.t == var || as->dst.t == reg) {
					shouldConnect = false;
				}
			}

		}
		if (shouldConnect) {
			set<node> killSet = processToNode(df->globalGenKill.at(i)->kill);
			set<node> outSet = processToNode(df->inOutPerLine.at(i)->out);
			for (node k : killSet) {
				df->i_graph->at(k).insert(outSet.begin(), outSet.end());
        			df->i_graph->at(k).erase(k);
			}
			for (node o : outSet) {
				df->i_graph->at(o).insert(killSet.begin(), killSet.end());
        			df->i_graph->at(o).erase(o);
			}
		}
	}
}

void sopCase(DataFlowResult* df, Function f) {
	for (int i = 0; i < (f.lines).size(); i++) {
		Instruction *instruct = f.lines[i];
		if (instruct->type == Arith) {
			Arithmetic* ar = static_cast<Arithmetic*>(instruct);
			if ((ar->op == '<' || ar->op == '>') && ar->src.t == var) {
				node key = node{ar->src.str, false};
				df->i_graph->at(key).insert(register_nodes.begin(), register_nodes.end());
				node rcxNode = node{"rcx", true};
				df->i_graph->at(key).erase(rcxNode);
				for (node r : register_nodes) {
					if (!(r == rcxNode))
						df->i_graph->at(r).insert(key);
				}
			}
		}
	}
}

void printGraph (DataFlowResult* df) {
  for (auto it: *df->i_graph)
  {
     cout << it.first.name << " ";
     //cout << it.first.name << " (" << it.first.color << ") ";  // string (key)
     for (auto val : it.second) {
         cout << val.name << " ";
     }
     cout << endl;
  }
}

void compute_interference(DataFlowResult* liveness, Program p, Function* f) {

    liveness->i_graph = new map<node, set<node>>;
    liveness->i_graph = addNodes(registers, liveness->i_graph);

    for (inOut* IO : liveness->inOutPerLine) {
        liveness->i_graph = addNodes(IO->in, liveness->i_graph);
        liveness->i_graph = addNodes(IO->out, liveness->i_graph);
    }

    for (genKill* gk: liveness->globalGenKill){
	   set<node> killNodes = processToNode(gk->kill);
	   set<node> genNodes = processToNode(gk->gen);
   	   for (node n: killNodes){
	        liveness->i_graph->insert(pair<node, set<node>>(n, set<node>()));
  	   }
   	   for (node g: genNodes){
	        liveness->i_graph->insert(pair<node, set<node>>(g, set<node>()));
  	   }
    }

    connectKillOut(liveness, *f);
    sopCase(liveness, *f);
    //printGraph(liveness);
 }

}
